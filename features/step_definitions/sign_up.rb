require 'selenium-webdriver'
require 'rubygems'
require 'rspec'

driver = Selenium::WebDriver::Chrome::Service.driver_path="../features/chromedriver"

Given("prepare to sign up") do
  driver = Selenium::WebDriver.for :chrome
  driver.manage.window.resize_to(1440,900)
  driver.navigate.to "https://www.amazon.com/"
  driver.manage.timeouts.implicit_wait = 10
  driver.find_element(:xpath,"//*[@id='nav-link-accountList']").click
end

When("input valid name , email , password") do
  driver.manage.timeouts.implicit_wait = 10
  driver.find_element(:xpath,"//*[@id='createAccountSubmit']").click
  driver.manage.timeouts.implicit_wait = 10
  driver.find_element(:xpath,"//*[@id='ap_customer_name']").send_keys 'ini_nama_kamu'
  driver.find_element(:xpath,"//*[@id='ap_email']").send_keys 'ini_email_kamu@email.com'
  driver.find_element(:xpath,"//*[@id='ap_password']").send_keys '12345678'
  driver.find_element(:xpath,"//*[@id='ap_password_check']").send_keys '12345678'
  driver.find_element(:xpath,"//*[@id='continue']").click
end

Then("success sign up") do
  driver.manage.timeouts.implicit_wait = 10
  driver.find_element(:xpath,"//h1[contains(text(),'Verify email address')]")
end
