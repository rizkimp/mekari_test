require 'selenium-webdriver'
require 'rubygems'
require 'rspec'

driver = Selenium::WebDriver::Chrome::Service.driver_path="../features/chromedriver"

Given("prepare to sign in") do
  driver = Selenium::WebDriver.for :chrome
  driver.manage.window.resize_to(1440,900)
  driver.navigate.to "https://www.amazon.com/"
  driver.manage.timeouts.implicit_wait = 10
  driver.find_element(:xpath,"//*[@id='nav-link-accountList']").click
end

When("input valid email and password") do
  driver.manage.timeouts.implicit_wait = 10
  driver.find_element(:xpath,"//*[@id='ap_email']").send_keys 'tamanyanghijau@gmail.com'
  driver.find_element(:xpath,"//*[@id='continue']").click
  driver.manage.timeouts.implicit_wait = 10
  driver.find_element(:xpath,"//*[@id='ap_password']").send_keys '12345678'
  driver.find_element(:xpath,"//*[@id='signInSubmit']").click
end

Then("success sign in") do
  driver.manage.timeouts.implicit_wait = 10
  driver.find_element(:xpath,"//h1[contains(text(),'Authentication required')]")
end
